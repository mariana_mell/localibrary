
from django.contrib import admin
from django.urls import path, include
from axes.decorators import watch_login 


urlpatterns = [
    path('admin/', admin.site.urls),
    path('catalog/', include('catalog.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/login/', watch_login(admin.site.login_view)),
    
    
]
