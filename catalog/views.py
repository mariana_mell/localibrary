from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Livro, Reserva_livro, Autor
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import authenticate, login
from django.core.paginator import Paginator
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from datetime import date 
from datetime import timedelta 
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView,
PasswordResetConfirmView, PasswordResetCompleteView


#funcao login para logar o usuario e validacao de senha
@login_required 
def logar(request):
    if request.method == 'GET':
        return render(request, 'login.html')
    elif request.method == 'POST':
        nome = request.POST.get('nome')
        senha = request.POST.get('senha')
        confirmar_senha = request.POST.get('confirmar_senha')

        if not senha:
            messages.error(request, 'Insira uma senha')
            return render(request, 'login.html')

        if len(senha) < 8:
            messages.error(request, 'A senha deve ter 8 caracteres.')
            return render(request, 'login.html')

        elif senha != confirmar_senha:
            messages.error(request, 'As senhas devem ser iguais.')
            return render(request, 'login.html')
        
        user = authenticate(username=nome, password=senha)
        
        if not user:
            messages.error(request, 'nome ou senha invalidos.')
            return render(request, 'login.html')

        auth.login(request, user)
        messages.success(request, 'login realizado com sucesso.')
        return redirect('/cadastrar')


#funcao para cadastrar o usuario validando senha e usuario
@login_required 
def cadastrar(request):
    if request.method == 'GET':
        return render(request, 'cadastro.html')
    elif request.method == 'POST':
        nome = request.POST.get('nome')
        email = request.POST.get('email')
        senha = request.POST.get('senha')
        confirmar_senha = request.POST.get('confirmar_senha')
            
        user = User.objects.filter(username=nome)  

        if user:
            messages.error(request, 'Esse usuario ja existe.')
            return render(request, 'cadastro.html')

        if not senha:
            messages.error(request, 'Insira uma senha')
            return render(request, 'cadastro.html')
            
        if len(senha) <8:
            messages.error(request, 'A senha deve ter 8 caracteres.')
            return render(request, 'cadastro.html')
        
        elif senha != confirmar_senha:
            messages.error(request, 'As senhas devem ser iguais.')
            return render(request, 'cadastro.html')

        try:
            validate_email(email)
        except ValidationError:
            messages.error(request, 'Insira um email valido')
            return render(request, 'cadastro.html')

        user = User.objects.create_user(username=nome, email=email, password=senha)
        return redirect('/catalog')

        
#funcao catalog gerando contagem de objetos e de visitas de usuarios na livraria
#definindo variavel para a contagem de visitas de usuarios

num_visitas = 0

@login_required
def catalog(request):
    total_livros = Livro.objects.all().count()
    total_reserva = Reserva_livro.objects.all().count()
    total_reserva_disponivel = Reserva_livro.objects.filter(status_reserva='disponivel').count()
    numero_autores = Autor.objects.count()

    num_visitas +=1

    dados = {'total_livros': total_livros, 'total_reserva': total_reserva, 'total_reserva_disponivel': total_reserva_disponivel, 'numero_autores': numero_autores,
    'num_visitas': num_visitas,}

    return render(request, 'catalog.html', context=dados)


#funcao para listagem de livros total, incluindo os reservados pelo o usuario.
@login_required
def lista_livros(request):
    livros = Livro.objects.all()
    livros_reservados = Reserva_livro.objects.filter(status_reserva='reservado').count()

    paginator = Paginator(livros, 10)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    page_obj.livros_reservados = livros_reservados

    return render(request, 'lista_livro.html', {'livros': page_obj})


#funcao para exibir uma lista de autores 
@login_required
def lista_autores(request):
    autores = Autor.objects.all()

    paginator = Paginator(autores, 10)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, 'lista_autores.html', {'autores':page_obj})


#funcao para exibir detalhes de um autor 
@login_required
def detalhes_autor(request):
    try:
        autor_id = request.GET.get('autor_id')
        autor = Autor.objects.get(pk=autor_id)
        return render(request, 'detalhes_autor.html', {'autor':autor})
    except Autor.DoesNotExist:
        messages.error(request, 'Esse autor nao existe.')
        return render(request, 'detalhes_autor.html', {'autor':autor})


#funcao para o usuario realizar a reserva de um livro.

@login_required
def reserva_livro(request): 
    if request.method == 'POST':
        numero_isbn = request.POST.get('numero_isbn')
        try:
            livro = Reserva_livro.objects.get(numero_isbn=numero_isbn)
        except Reserva_livro.DoesNotExist:
            messages.error(request, 'O livro nao foi encontrado')
            return render(request, 'reserva_livro.html')

        if livro.status_reserva == 'emprestado':
            messages.error(request, 'Nao e possivel reservar esse livro no momento.')
            return render(request, 'reserva_livro.html')

        elif livro.status_reserva == 'indisponivel':
            messages.error(request, f'O livro, {livro} esta indisponivel no momento.')
            return render(request, 'reserva_livro.html')

        else: 
            if livro.data_devolucao < date.today():
                reserva_vencida = Reserva_livro.objects.filter(livro=livro, borrower=request.user,
                status_reserva='reservado', data_devolucao__lt=date.today()).first()
                if reserva_vencida:
                    nova_data_devolucao = date.today() + timedelta(days=7)
                    reserva_vencida.data_devolucao = nova_data_devolucao 
                    reserva_vencida.save()
                    messages.success(request, f'Sua reserva vencida do livro {livro} foi renovada com sucesso.')
                else:
                    messages.error(request, f'A reserva do livro {livro} esta vencida.')
                    return render(request, 'reserva_livro.html')
            else:
                reserva_existente = Reserva_livro.objects.filter(livro=livro, borrower=request.user, status_reserva='reservado')
                if reserva_existente:
                    messages.error(request, f'Voce ja possui uma reserva do livro {livro}')
                    return render(request, 'reserva_livro.html')
        
            livro.status_reserva = 'disponivel'
            reserva = Reserva_livro(livro=livro, borrower=request.user, status_reserva='reservado')
            reserva.save()
            messages.success(request, f'Seu livro, {livro} foi reservado com sucesso.')

        return redirect('lista_livros/')

    return render(request, 'reserva_livro.html')


#todo:recupercao de senha

#funcao para o usuario remover uma reserva de livro
@login_required
def cancelar_reserva(request, livro, data_devolucao):
    livro_reservado = Reserva_livro.objects.filter(livro=livro, borrower=request.user, status_reserva='reservado')
    if livro_reservado:
        livro_reservado.delete()
        messages.success(request, f'Reserva do livro {livro} removida com sucesso.')
        return redirect('lista_livros/')
    try:
        livro_vencido = Reserva_livro.objects.get(status_reserva='reservado', borrower=request.user, data_devolucao=data_devolucao)
        if livro_vencido.data_devolucao < date.today():
            messages.error(request, f'A reserva do livro {livro} esta vencida, nao e possivel remove-la.')
            return redirect('lista_livros/')
    except Reserva_livro.DoesNotExist: 
        messages.error(request, f'A reserva do livro {livro} nao existe.')
        return redirect('lista_livros/')

    livro_reservado = Reserva_livro.objects.filter(livro=livro, borrower=request.user, status_reserva='reservado', data_devolucao=data_devolucao)
    if livro_reservado.exists(): 
        if livro_reservado[0].status_reserva == 'cancelado':
            livro_reservado.update(status_reserva='reservado')
            messages.success(request, f'A reserva do livro {livro} foi reativada com sucesso.')

        if livro_reservado[0].data_devolucao < date.today():
            messages.error(request, f'A reserva do livro {livro} esta vencida. Nao e possivel renova-la.')
            return redirect('lista_livros.html')

        else:
            messages.error(request, f'O livro {livro} ja esta reservado.')
            return redirect('lista_livros/')

    return redirect('reservas_usuario/')


#todo: exibicao de um grafico dashboard da estatistica
#funcao para exibicao de lista de livro emprestados ao usuario 
@login_required 
def reservas_usuario(request):
    reservas = Reserva_livro.objects.filter(borrower=request.user, status_reserva='emprestado').order_by('data_emprestimo', 'data_devolucao')
   
    paginator = Paginator(reservas, 10)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, 'reservas_usuario.html', {'reservas':page_obj})


#funcao para bibliotecarios da livraria
@staff_member_required
def bibliotecarios(request):
    livros_usuario = Reserva_livro.objects.all()
    nomes_usuario = []
    for reserva in livros_usuario:
        nomes_usuario.append(reserva.borrower.username)
    return render(request, 'bibliotecarios.html')


#definindo classes da redefinicao de senha do usuario
class MyPasswordResetView(PasswordResetView):
    template_name='password_reset.html'


class MyPasswordResetDoneView(PasswordResetDoneView):
    template_name='password_reset_done.html'
    

class MyPasswordResetConfirmView(PasswordResetConfirmView):
    template_name='password_reset_confirm.html'


class MyPasswordResetCompleteView(PasswordResetCompleteView):
    template_name='password_reset_complete_view.html'

