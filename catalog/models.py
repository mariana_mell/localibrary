from django.db import models
from django.urls import reverse
import uuid

#classe representando o genero do livro
class Genero_livro(models.Model):
    genero = models.CharField(max_length=200, help_text='Insira um genero de livro(ex. Romance)')

    def __str__(self):
        return self.genero


#classe para o idioma do livro 
class Idioma(models.Model):
    idioma = models.CharField(max_length=20, help_text='Idioma do livro(ex. Ingles, Frances...)')

    def __str__(self):
        return self.idioma

#classe representando dados do livro, gerando uma lista com 3 primeiros generos
class Livro(models.Model):
    titulo = models.CharField(max_length=200)
    autor = models.ForeignKey('Autor', on_delete=models.SET_NULL, related_name='livro')
    sumario = models.TextField(max_length=100, help_text='Insira uma breve descricao do livro')
    numero_isbn = models.CharField('numero_isbn', max_length= 13 ,help_text='13 Character <a href="https://www.isbn-international.org/content/what-isbn">ISBN number</a>' )
    genero = models.ManyToManyField('Genero_livro', help_text='Selecione um genero para esse livro(ex.Aventura)')
    idioma = models.ForeignKey('Idioma', on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ['titulo', 'autor', 'genero']

    def display_genero(self):
        return ','.join([genero.nome for genero in self.genero.all()[:3]])

    display_genero.short_description= 'genero'

    def get_absolute_url(self):
        return reverse('reserva_livro', args=[str(self.id)])

    def __str__(self):
        return self.titulo 

#classe representada a copia de um livro e o status da reserva
class Reserva_livro(models.Model):
    livro = models.ForeignKey('Livro', on_delete=models.SET_NULL, null=True)
    editora_livro = models.CharField(max_length=200)
    data_devolucao = models.DateField(null=True)

    status_reserva = (
        ('indisponivel', 'Indisponivel'), ('emprestado', 'Emprestado'), ('disponivel' ,'Disponivel'), ('reservado' ,'Reservado'),
    )

    status = models.CharField(max_length=20, choices=status_reserva, blank=True,
     default='indisponivel', help_text='Status da reserva',
    )

    class Meta:
        ordering = ['livro', 'data_devolucao']

    def __str__(self):
        return f'{self.livro}'

#classe para autor do livro armazenando nome e data
class Autor(models.Model):
    nome_autor = models.CharField(max_length=100)
    data_nascimento = models.DateField(null=True, blank=True)

    class Meta:
        ordering = ['nome_autor', 'data_nascimento']

    def get_absolute_url(self):
        return reverse('detalhes_autor', args= [str(self.id)])

    def __str__(self):
        return f'{self.nome_autor} ({self.data_nascimento})'


#classe login armazenando os dados do usuario 
class Login(models.Model):
    nome = models.CharField(max_length=30)
    senha = models.CharField(max_length=8)
    confirmar_senha = models.Charfield(max_length=8)

    def __str__(self):
        return self.nome


#classe cadastrar armazenando dados do usuario
class Cadastro(models.Model):
    nome = models.CharField(max_length=30)
    email = models.CharField(max_length=100)
    confirmar_senha = models.CharField(max_length=8)
    senha = models.CharField(max_length=8)

    def __str__(self):
        return self.nome
   