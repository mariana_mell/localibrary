from django.contrib import admin
from catalog.models import Autor, Livro, Genero_livro, Reserva_livro, Logar, Cadastrar

admin.site.register(Autor)
admin.site.register(Livro)
admin.site.register(Genero_livro)
admin.site.register(Reserva_livro)
admin.site.register(Logar)
admin.site.register(Cadastrar)

