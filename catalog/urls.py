from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('logar/', views.logar, name='logar'),
    path('cadastrar/', views.cadastrar, name='cadastrar'),
    path('catalog/', views.catalog, name='catalog'),
    path('lista_livros/', views.lista_livros, name='lista_livros'),
    path('lista_autores/', views.lista_autores, name= 'lista_autores'),
    path('reserva_livro/', views.reserva_livro, name='reserva_livro'),
    path('detalhes_autor/', views.detalhes_autor, name= 'detalhes_autor'),
    path('cancelar_reserva/', views.cancelar_reserva, name='cancelar_reserva'),
    path('reservas_usuario/', views.reservas_usuario, name= 'reservas_usuario'),
    path('password_reset/', auth_views(template_name='password_reset.html'), name='password_reset'),
    path('password_reset_done/', auth_views(template_name='password_reset_done.html'), name='password_reset_done'),
    path('password_reset_confirm/', auth_views(template_name='password_reset_confirm.html'), name='password_reset_confirm'),
    path('password_reset_complete/', auth_views(template_name='password_reset_complete.html'), name='password_reset_complete'),
    path('password_reset_email/', auth_views(template_name='password_reset_email.html'), name='password_reset_email'),
    
]


